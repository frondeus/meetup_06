pub use crate::try5::*;

#[derive(Debug, PartialEq)]
enum Cons {
    Nil,
    Pair(Box<Cons>, Box<Cons>)
}

fn nil<'a>() -> impl Parser<'a, Output = Cons> {
    "()".map(|_| Cons::Nil)
}

fn pair<'a>() -> impl Parser<'a, Output = Cons> {
    ('(', cons(), '.', cons(), ')').map(|(_, lhs, _, rhs, _)| {
        Cons::Pair(Box::new(lhs), Box::new(rhs))
    })
}


/*
error[E0275]: overflow evaluating the requirement `impl try4::Parser`
= help: consider adding a `#![recursion_limit="128"]` attribute to your crate

fn cons<'a>() -> impl Parser<'a, Output = Cons> {
or(nil(), pair())
    }
    */

/* Ok so basically here is an error. It will overflow stack on runtime!
fn cons<'a>() -> impl Parser<'a, Output = Cons> {
    let p = _cons();
    move |input| {
        p.parse(input)
    }
}

You need to do this in that way:
*/

fn cons<'a>() -> impl Parser<'a, Output = Cons> {
    move |input| { //By having this closure we "break" stack overflow infinite loop
        or(nil(), pair())
            .parse(input)
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_char() {
        let parser = 'c';
        let result = parser.parse("cd").unwrap();

        assert_eq!(result, ('c', "d"));
    }

    #[test]
    fn test_or() {
        let parser = or("c", "22");
        let result = parser.parse("c22").unwrap();

        assert_eq!(result, ("c", "22"));

        let result2 = parser.parse("22c").unwrap();

        assert_eq!(result2, ("22", "c"));
    }

    #[test]
    fn test_nil() {
        let parser = nil();

        let result = parser.parse("()").unwrap();

        assert_eq!(result, (Cons::Nil, ""));
    }

    #[test]
    fn test_not_nil() {
        let parser = nil();

        let result = parser.parse("(!").unwrap_err();

        assert_eq!(result, "Expected `()` found `(!`");
    }

    #[test]
    fn test_pair() {
        let parser = pair();

        let result = parser.parse("(().())").unwrap();

        assert_eq!(result, (Cons::Pair(Box::new(Cons::Nil), Box::new(Cons::Nil)), ""));
    }

    #[test]
    fn test_not_pair() {
        let parser = pair();

        let result = parser.parse("(...)").unwrap_err();

        assert_eq!(result, "Expected `(` found `.`");
    }
    
}

type Input<'a> = &'a str;
type ParserOutput<'a, T> = Result<(T, Input<'a>), String>;

trait Parser<'a> {
    type Output;

    fn parse(&self, input: Input<'a>) -> ParserOutput<'a, Self::Output>;

    fn map<O2, F>(self, f: F) -> impl Parser<'a, Output = O2>
    where F: Fn(Self::Output) -> O2,
          Self: Sized {
        map(self, f)
    }

}

impl<'a, F, O> Parser<'a> for F
where F: Fn(Input<'a>) -> ParserOutput<'a, O> {
    type Output = O;

    fn parse(&self, input: Input<'a>) -> ParserOutput<'a, Self::Output> {
        self(input)
    }
}

//----

fn take<'a>(len: usize) -> impl Parser<'a, Output = &'a str> {
    move |input: Input<'a>| {
        Ok((&input[0..len], &input[len..]))
    }
}

fn char_where<'a, F>(f: F) -> impl Parser<'a, Output = char>
where F: Fn(char) -> bool {
    let parser = take(1);
    move |input| {
        let (first, rest) = parser.parse(input)?;
        let first = first.chars().next().unwrap();
        if f(first) {
            Ok((first, rest))
        }
        else {
            Err(format!("Function returned false"))
        }
    }
}

fn map<'a, P, O1, O2, F>(parser: P, f: F) -> impl Parser<'a, Output = O2>
where P: Parser<'a, Output = O1>, F : Fn(O1) -> O2 {
    move |input| {
        let (o1, r1) = parser.parse(input)?;
        let o2 = f(o1);
        Ok((o2, r1))
    }
}

#[cfg(test)]
mod tests {
    use super::Parser;

    fn number<'a>() -> impl super::Parser<'a, Output = u32> {
        let digit = super::char_where(|c| c.is_digit(10));

        digit.map(|c: char| c.to_digit(10).unwrap())
    }

    #[test]
    fn it_works() {
        let parser = number();

        let result = parser.parse("5");

        assert_eq!(result, Ok((5, "")));
    }
}

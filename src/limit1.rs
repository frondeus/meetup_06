pub use crate::try4::*;

fn value<'a, T>(value: T) -> impl Parser<'a, Output = T>
where T: Copy {
    move |input: Input<'a>| {
        Ok((value, input))
    }
}

fn number<'a>() -> impl Parser<'a, Output = u32> {
    let digit = char_where(|c| c.is_digit(10), "digit");

    digit.map(|c: char| c.to_digit(10).unwrap())
}

/*
error[E0308]: if and else have incompatible types
expected opaque type, found a different opaque type
|
= note: expected type `impl try4::Parser<'_>` (opaque type)
found type `impl try4::Parser<'_>` (opaque type)

fn if_opt<'a>(flag: bool) -> impl Parser<'a, Output = u32> {
    if flag {
        value::<u32>(1)
    }
    else {
        number()
    }
}
*/

mod fix {
    use super::*;

    fn if_opt<'a>(flag: bool) -> Box<dyn Parser<'a, Output = u32> + 'a> {
        if flag {
            Box::new(value::<u32>(1))
        }
        else {
            Box::new(number::<'a>())
        }
    }

    mod tests {
        #[test]
        fn it_works() {
            let parser = super::if_opt(true);

            let result = parser.parse("5");

            assert_eq!(result, Ok((1, "5")));
        }

        #[test]
        fn it_works_2() {
            let parser = super::if_opt(false);

            let result = parser.parse("5");

            assert_eq!(result, Ok((5, "")));
        }
    }
}

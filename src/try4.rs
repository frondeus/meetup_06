pub type Input<'a> = &'a str;
pub type ParserOutput<'a, T> = Result<(T, Input<'a>), String>;

pub trait Parser<'a> {
    type Output;

    fn parse(&self, input: Input<'a>) -> ParserOutput<'a, Self::Output>;

    fn map<O2, F>(self, f: F) -> Map<Self, F>
    where F: Fn(Self::Output) -> O2,
          Self: Sized {
        Map::new(self, f)
    }

}

impl<'a, F, O> Parser<'a> for F
where F: Fn(Input<'a>) -> ParserOutput<'a, O> {
    type Output = O;

    fn parse(&self, input: Input<'a>) -> ParserOutput<'a, Self::Output> {
        self(input)
    }
}

//----

pub struct Map<P, F> {p: P, f: F}

impl<P, F> Map<P, F> {
    pub fn new(p: P, f: F) -> Self {
        Map {p, f}
    }
}

impl <'a, O, P, F> Parser<'a> for Map<P, F>
where P: Parser<'a>,
      F: Fn(P::Output) -> O,
      Self: Sized {
    type Output = O;

    fn parse(&self, input: Input<'a>) -> ParserOutput<'a, Self::Output> {
        let f = &self.f;
        let (o1, r1) = self.p.parse(input)?;
        let o2 = f(o1);
        Ok((o2, r1))
    }
}

//----

pub fn take<'a>(len: usize) -> impl Parser<'a, Output = &'a str> {
    move |input: Input<'a>| {
        Ok((&input[0..len], &input[len..]))
    }
}

use std::fmt::Display;

pub fn char_where<'a, F, S>(f: F, desc: S) -> impl Parser<'a, Output = char>
where F: Fn(char) -> bool,
    S: Display
{
    let parser = take(1).map(|f| f.chars().next());
    move |input| {
        let (first, rest) = parser.parse(input)?;

        match first {
            Some(letter) if f(letter) => Ok((letter, rest)),
            Some(letter) => Err(format!("Expected {} found `{}`", desc, letter)),
            None => Err(format!("Expected {} found end of file", desc))
        }
    }
}

pub fn map<'a, P, O1, O2, F>(parser: P, f: F) -> impl Parser<'a, Output = O2>
where P: Parser<'a, Output = O1>, F : Fn(O1) -> O2 {
    Map::new(parser, f)
}

#[cfg(test)]
mod tests {
    use super::Parser;

    fn number<'a>() -> impl super::Parser<'a, Output = u32> {
        let digit = super::char_where(|c| c.is_digit(10), "digit");

        digit.map(|c: char| c.to_digit(10).unwrap())
    }

    #[test]
    fn it_works() {
        let parser = number();

        let result = parser.parse("5");

        assert_eq!(result, Ok((5, "")));
    }
}
